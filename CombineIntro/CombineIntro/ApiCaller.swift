//
//  ApiCaller.swift
//  CombineIntro
//
//  Created by ang on 2023/1/9.
//

import Foundation
import Combine

class ApiCaller {
    static let share = ApiCaller()
    
    func fetchCompainies() -> Future<[String], Error> {
        return Future { promixe in
            DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                promixe(.success(["Apple", "Google", "Microsoft", "Facebook"]))
            }
        }
    }
}
