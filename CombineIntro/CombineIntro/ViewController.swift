//
//  ViewController.swift
//  CombineIntro
//
//  Created by ang on 2023/1/9.
//

import UIKit
import Combine

class ViewController: UIViewController {
    private var tableView: UITableView = {
        let table = UITableView()
        table.register(MyCustomTableViewCell.self, forCellReuseIdentifier: "Cell")
        return table
    }()
    // 方法1
    //    var observer: AnyCancellable?
    // 方法2
    var observers: [AnyCancellable] = []
    
    private var models: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.frame = view.bounds
        // 方法1
        //        observer = ApiCaller.share.fetchCompainies()
        //            .receive(on: DispatchQueue.main)
        //            .sink(receiveCompletion: { completion in
        //                switch completion {
        //                case .finished:
        //                    print("Finished")
        //                case .failure(let error):
        //                    print("error:\(error)")
        //                }
        //            }, receiveValue: { [weak self] value in
        //                print("value: \(value)")
        //                self?.models = value
        //                self?.tableView.reloadData()
        //                print("models: \(self?.models)")
        //            })
        // 方法2
        ApiCaller.share.fetchCompainies()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    print("Finished")
                case .failure(let error):
                    print("error:\(error)")
                }
            }, receiveValue: { [weak self] value in
                print("value: \(value)")
                self?.models = value
                self?.tableView.reloadData()
                print("models: \(self?.models)")
            }).store(in: &observers)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? MyCustomTableViewCell else {
            fatalError()
        }
        //        cell.titleLabel.text = models[indexPath.row]
        // 方法2
        cell.action.sink { string in
            print("cell.action.sink, \(string)")
        }.store(in: &observers)
        
        return cell
    }
}

class MyCustomTableViewCell: UITableViewCell {
    private let button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .systemPink
        button.setTitle("Button", for: .normal)
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    let action = PassthroughSubject<String, Never>()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(button)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func didTapButton() {
        action.send("Cool! Button was tapped.")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        button.frame = CGRect(x: 10, y: 3, width: contentView.frame.self.width - 20, height: contentView.frame.size.height - 6)
    }
}

// Ref: https://www.youtube.com/watch?v=hbY1KTI0g70
